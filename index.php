<?php
    
ob_start();
// Adicionar
require_once "config/config.php";
require_once $_SERVER['DOCUMENT_ROOT'] .  "/".APP."/config/loader.php";
$controller = $_GET['control'];
$view = $_GET['pag'];
$views = $_SERVER['DOCUMENT_ROOT'] . "/".APP."/view";
// Verifica qual controller
$link = $views . "/" . $controller . "/" . $view . ".php";
if (file_exists($link)) {

    switch ($view) {
        case "list":
            include $link;
            break;
        case "details":
            include $link;
            break;
        case "zform":
            include $link;
            break;
        case "newpass":
            include $link;
            break;
        case "locacao":
            include $link;
            break;
        case "locar":
            include $link;
            break;
        case "locacoes":
            include $link;
            break;
        default:
            include $views . "/filmes/locacao.php";
    }
}else{
     include "/filmes/locacao.php";
}

if (($_SERVER['REQUEST_METHOD'] == 'GET') && (!empty($_GET['msg']))) {

    echo "<label style='color:red'>" . $_GET['msg'] . "</label>";
}

$pagemaincontent = ob_get_contents();
ob_end_clean();
require_once 'public/master.php';
?>

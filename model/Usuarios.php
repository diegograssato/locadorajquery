
<?php

/**
 * Modelo
 */
class Usuarios {

    /**
     *  Construtor Principal da Class, é o inicio da classe
     */
    private $_db = NULL;

    function __construct() {
        $this->_db = new conexao();
    }

    /**
     * Insere os dados do contato
     * @param type $data[]
     * @return id
     */
    public function insert($usuario) {
        // logica para salvar contato no banco
        $sql = "INSERT INTO clientes (nome,email,nivel,senha)
                    VALUES (?,?,?,?);";
        $query = $this->_db->prepare($sql);
        $query->bindParam(1, $usuario->getNome(), PDO::PARAM_STR);
        $query->bindParam(2, $usuario->getEmail(), PDO::PARAM_STR);
        $query->bindParam(3, $usuario->getNivel(), PDO::PARAM_INT);
        $query->bindParam(4, $usuario->getSenha(), PDO::PARAM_STR);

        $executa = $query->execute();
        if ($executa) {

            return $this->_db->lastInsertId();
        } else {
            return false;
        }
    }

    /**
     * Atualza os dados do contato
     * @param type $data[]
     * @return id
     */
    public function update($usuario) {
        $sql = "UPDATE clientes SET nome=:nome,
                    email=:email WHERE id=:id";
        $query = $this->_db->prepare($sql);
        $query->bindParam(":id", $usuario->getId(), PDO::PARAM_INT);
        $query->bindParam(":nome", $usuario->getNome(), PDO::PARAM_STR);
        $query->bindParam(":email", $usuario->getEmail(), PDO::PARAM_STR);
        $executa = $query->execute();
        if ($executa) {
            return $usuario->getId();
        } else {
            return false;
        }
    }

    /**
     * Atualza os dados do contato
     * @param type $data[]
     * @return id
     */
    public function newPass($usuario) {
        $sql = "UPDATE clientes SET senha=:senha WHERE id=:id";
        $query = $this->_db->prepare($sql);
        $query->bindParam(":id", $usuario->getId(), PDO::PARAM_INT);
        $query->bindParam(":senha", $usuario->getSenha(), PDO::PARAM_STR);
        $executa = $query->execute();
        if ($executa) {
            return $usuario->getId();
        } else {
            return false;
        }
    }

    /**
     * Remove um contato
     * @param type $id
     * @return boolean
     */
    public function remove($id) {
        // logica para remover cliente do banco
        $query = $this->_db->prepare("DELETE FROM clientes WHERE id=?");
        $query->bindParam(1, $id, PDO::PARAM_INT);
        $executa = $query->execute();
        if ($executa) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Faz a busca de um contato pelo seu ID
     * @param type $id
     * @return data[]
     */
    public function findByID($id) {
        // logica para listar toodos os clientes do banco
        $query = $this->_db->prepare("SELECT * FROM clientes WHERE id=? AND nivel = 1");
        $query->bindParam(1, $id, PDO::PARAM_INT);
        $executa = $query->execute();
        if ($executa) {
            $rows = $query->fetch();
        } else {
            echo 'Erro ao bucar os dados';
        }
        return $rows;
    }

    /**
     * Lista todos os contatos
     * @return data[]
     */
    public function findAll() {
        // logica para listar toodos os clientes do banco
        $query = $this->_db->query("SELECT * FROM clientes WHERE nivel = 1")->fetchAll();
        return $query;
        echo "Exibindo tuso";
    }

    /**
     * @param type $dados
     * @return data[]
     */
    public function find($dados) {
        // logica para listar toodos os clientes do banco
        $query = $this->_db->prepare("SELECT * FROM clientes WHERE
                                 (nome LIKE ?
                                OR email LIKE ? )AND nivel=1");
        $query->bindValue(1, "%$dados%", PDO::PARAM_STR);
        $query->bindValue(2, "%$dados%", PDO::PARAM_STR);
        $executa = $query->execute();
        if ($executa) {
            $rows = $query->fetchAll();
        } else {
            echo 'Erro ao bucar os dados';
        }
        return $rows;
    }

    /**
     * @return boolean
     */
    public function autenticacao($usuario) {
        // logica para remover cliente do banco
        $query = $this->_db->prepare("SELECT * FROM clientes WHERE email=? AND senha=?");
        $query->bindParam(1, $usuario->getEmail(), PDO::PARAM_STR);
        $query->bindParam(2, $usuario->getSenha(), PDO::PARAM_STR);
        $executa = $query->execute();
        if ($executa) {
            $rows = $query->fetchAll();
            if (count($rows) > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            echo 'Erro ao bucar os dados';
        }
    }

}

?>

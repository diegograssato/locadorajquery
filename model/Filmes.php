 
<?php
/**
 * Modelo 
 */
class Filmes {

    /**
     *  Construtor Principal da Class, é o inicio da classe
     */
    private $_db=NULL;
    
    function __construct() {
        $this->_db = new conexao();          
    }

    /**
     * Insere os dados do contato
     * @param type $data[]
     * @return id
     */ 
    public function insert($filme) {
        // logica para salvar contato no banco
        $sql = "INSERT INTO filmes (nome,id_categoria,resumo,sinopse, destaque, status, quantidade)  
                    VALUES (?,?,?,?,?,?,?)";
        $query = $this->_db->prepare($sql);
        $query->bindParam(1, $filme->getNome() , PDO::PARAM_STR);
        $query->bindParam(2, $filme->getCategoria(), PDO::PARAM_INT);
        $query->bindParam(3, $filme->getResumo(), PDO::PARAM_STR);
        $query->bindParam(4, $filme->getSinopse(), PDO::PARAM_STR);
        $query->bindParam(5, $filme->getDestaque(), PDO::PARAM_INT);
        $query->bindParam(6, $filme->getStatus(), PDO::PARAM_INT);
        $query->bindParam(7, $filme->getQuantidade(), PDO::PARAM_INT);
        $executa = $query->execute();
        
        if($executa){
            return   $this->_db->lastInsertId();
        }else{
            return false;
        } 
    }
    
    public function locar($dados) {
        // logica para salvar contato no banco
        $sql = "INSERT INTO locacao (status,id_filme,id_cliente)  
                    VALUES (1,?,?)";
         
        $query = $this->_db->prepare($sql);
        $query->bindParam(1, $dados['id_filme'], PDO::PARAM_INT);
        $query->bindParam(2, $dados['id_cliente'], PDO::PARAM_INT);
        $executa = $query->execute();
      
        if($executa){
            return   $this->_db->lastInsertId();
        }else{
            return false;
        } 
    }

    /**
     * Atualza os dados do contato
     * @param type $data[]
     * @return id
     */
    public function update($filme) {
        $sql = "UPDATE filmes SET nome=:nome,id_categoria=:categoria,
                    resumo=:resumo,sinopse=:sinopse,destaque=:destaque,
                    status=:status,quantidade=:quantidade
                    WHERE id=:id";
        $query = $this->_db->prepare($sql);
        $query->bindParam(":id", $filme->getId() , PDO::PARAM_INT);
        $query->bindParam(":nome", $filme->getNome() , PDO::PARAM_STR);
        $query->bindParam(":categoria", $filme->getCategoria(), PDO::PARAM_INT);
        $query->bindParam(":resumo", $filme->getResumo(), PDO::PARAM_STR);
        $query->bindParam(":sinopse", $filme->getSinopse(), PDO::PARAM_STR);
        $query->bindParam(":destaque", $filme->getDestaque(), PDO::PARAM_INT);
        $query->bindParam(":status", $filme->getStatus(), PDO::PARAM_INT);
        $query->bindParam(":quantidade", $filme->getQuantidade(), PDO::PARAM_INT);
        $executa = $query->execute();
        if($executa){
            return $filme->getId();
        }else{
            return false;
        }
    }

    /**
     * Remove um contato
     * @param type $id
     * @return boolean
     */
    public function remove($id) {
        // logica para remover cliente do banco
        $query = $this->_db->prepare("DELETE FROM filmes WHERE id=?");
        $query->bindParam(1, $id , PDO::PARAM_INT);
        $executa = $query->execute();
        if($executa){
            return true ;
        }else{
            return false;
        }
    }
    
   /**
    * Faz a busca de um contato pelo seu ID
    * @param type $id
    * @return data[]
    */
   public function findByID($id) {
        // logica para listar toodos os clientes do banco
        $query = $this->_db->prepare("SELECT * FROM filmes WHERE id=?");
        $query->bindParam(1, $id , PDO::PARAM_INT);
        $executa = $query->execute();
        if($executa){
            $rows = $query->fetch();
        }else{
           echo 'Erro ao bucar os dados';
       }
       return $rows;
      
    }
    
    /**
     * Lista todos os contatos
     * @return data[]
     */
    public function findAll() {
        // logica para listar toodos os clientes do banco
        $query =  $this->_db->query("SELECT * FROM filmes")->fetchAll();
        return $query;
    }
    
    /**
     * @param type $dados
     * @return data[]
     */
    public function find($dados) {
        // logica para listar toodos os clientes do banco
        $query = $this->_db->prepare("SELECT * FROM filmes WHERE 
                                 (nome LIKE ? 
                                OR resumo LIKE ? )");
        $query->bindValue(1, "%$dados%", PDO::PARAM_STR);
        $query->bindValue(2, "%$dados%", PDO::PARAM_STR);
        $executa = $query->execute();
        if($executa){
            $rows = $query->fetchAll();
        }else{
           echo 'Erro ao bucar os dados';
       }
       return $rows;
    }
    
    
    public function findLocacao($dados) {
        $query = $this->_db->prepare("SELECT f.*,(SELECT count(*) FROM locacao l WHERE l.id_filme = f.id AND l.status = 1) as qLocados,
        ( f.quantidade - (SELECT count(*) FROM locacao l WHERE l.id_filme = f.id AND l.status = 1) ) as livre
        FROM locacao l RIGHT JOIN filmes f ON  l.id_filme=f.id WHERE f.status = 1 AND nome LIKE ? GROUP BY l.id_filme ORDER BY qLocados DESC;");
        $query->bindValue(1, "%$dados%", PDO::PARAM_STR);
        $executa = $query->execute();
        if($executa){
            $rows = $query->fetchAll();
        }else{
           echo 'Erro ao bucar os dados';
       }
       return $rows;
    }
    
    public function clienteLocacao($id) {
        $query = $this->_db->prepare("SELECT l.id as LC,l.id_cliente  as cliente,l.locacao, l.status as statusLocado, l.devolucao,f.* 
        FROM locacao l RIGHT JOIN filmes f ON  l.id_filme=f.id WHERE l.id_cliente = ? ORDER BY l.status ASC;");
        $query->bindValue(1, $id, PDO::PARAM_INT);
        $executa = $query->execute();
        if($executa){
            $rows = $query->fetchAll();
        }else{
           echo 'Erro ao bucar os dados';
       }
       return $rows;
    }
    
    public function baixar($id) {
        $query = $this->_db->prepare("UPDATE locacao SET status=2, devolucao=NOW() WHERE id=?");
        $query->bindParam(1, $id , PDO::PARAM_INT);
        $executa = $query->execute();
        if($executa){
            return true ;
        }else{
            return false;
        }
    }
    

}

?>

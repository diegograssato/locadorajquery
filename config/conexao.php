<?php
// Remover
//require "config.php";

class conexao extends PDO {
    // -- São variaveis locais
    private $_database = DATABASE;
    private $_hostname = SERVER;
    private $_user = USERNAME;
    private $_password = PASSWORD;
    public  $handle = null;
    function __construct() { 
        // -- Controles excessoes
        try {
            if ($this->handle == null) {
                $dbh = parent::__construct("mysql:host=$this->_hostname;
                    dbname=$this->_database", $this->_user, $this->_password);
                $this->handle = $dbh;
                return $this->handle;
            }
        } catch (PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage();
            return false;
        }
    }

    function __destruct() {
        $this->handle = NULL;
    }

}

?>

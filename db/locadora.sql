-- MySQL dump 10.13  Distrib 5.5.31, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: locadora
-- ------------------------------------------------------
-- Server version	5.5.31-1~dotdeb.0

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categoria`
--

DROP TABLE IF EXISTS `categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `descricao` text,
  `criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria`
--

LOCK TABLES `categoria` WRITE;
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` VALUES (1,'AÃ§Ã£o','Filmes de AÃ§Ã£o','2013-07-18 21:29:24'),(2,'Terror','Filmes de terror','2013-07-18 21:29:24'),(5,'Raad','Raaa','2013-08-09 18:19:38');
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `senha` varchar(1000) DEFAULT NULL,
  `nivel` int(11) NOT NULL,
  `criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `nome_UNIQUE` (`nome`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COMMENT='\n';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (26,'Otavio','diego@kees.com.br','',2,'2013-07-28 12:31:55'),(31,'Diego Pereira Grassato','diego.grassato@gmail.com','',2,'2013-07-28 16:06:24'),(47,'Reinaldos','rscatanduva@uol.com.br','c4ca4238a0b923820dcc509a6f75849b',1,'2013-07-28 19:05:46'),(49,'Rafael','rafael_taka@msn.com','rafa2011',1,'2013-07-29 02:57:49'),(50,'Cleber','cleberof@msn.com','12345',1,'2013-07-29 02:57:57');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filmes`
--

DROP TABLE IF EXISTS `filmes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filmes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `resumo` text NOT NULL,
  `sinopse` text NOT NULL,
  `destaque` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `quantidade` int(11) NOT NULL DEFAULT '1',
  `criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_categoria` (`id_categoria`),
  CONSTRAINT `fk_categoria` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filmes`
--

LOCK TABLES `filmes` WRITE;
/*!40000 ALTER TABLE `filmes` DISABLE KEYS */;
INSERT INTO `filmes` VALUES (32,'Poeira em Alto Mar',1,'Bem se agora nao for esta parada a coisa vai ficar afro brasileira rsrsr','                                                                                                            <p>Filme baseado em um baseado que o direto deve de te fumado e ficado muito doido dai resolveu fazer o trem funcionar mais sei la acho que o camarada tava mesmo muito doidao</p><p><img alt=\"\" align=\"top\" /> </p>                                                            ',1,1,2,'2013-07-18 21:31:48'),(33,'Terra',2,'Bem se agora nao for esta parada a coisa vai ficar afro brasileira rsrsr','                                    <p>Filme baseado em um baseado que o direto deve de te fumado e ficado muito doido dai resolveu fazer o trem funcionar mais sei la acho que o camarada tava mesmo muito doidao</p><p><img alt=\"\" align=\"top\" /> </p>                    ',1,1,10,'2013-07-18 21:31:48'),(40,'assaas',2,'dddddddd','                                                                                                                                                                                                                                                                                                                                                                                                            ddddddddddddd                                                                                                                                                                                                                                                                                       ',1,1,12,'2013-07-29 14:27:24');
/*!40000 ALTER TABLE `filmes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locacao`
--

DROP TABLE IF EXISTS `locacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL DEFAULT '0',
  `id_cliente` int(11) NOT NULL,
  `id_filme` int(11) NOT NULL,
  `devolucao` datetime DEFAULT NULL,
  `locacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_loca_filme` (`id_filme`),
  KEY `fk_loca_cliente` (`id_cliente`),
  CONSTRAINT `fk_loca_cliente` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_loca_filme` FOREIGN KEY (`id_filme`) REFERENCES `filmes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locacao`
--

LOCK TABLES `locacao` WRITE;
/*!40000 ALTER TABLE `locacao` DISABLE KEYS */;
INSERT INTO `locacao` VALUES (1,2,31,32,'2013-08-05 22:18:44','2013-07-29 14:44:18'),(4,2,26,32,'2013-08-05 22:16:47','2013-07-29 14:44:53'),(5,2,31,33,'2013-08-05 22:18:43','2013-07-29 14:46:59'),(6,2,26,33,'2013-07-30 11:46:59','2013-07-29 14:46:59'),(7,2,26,33,'2013-08-05 22:16:34','2013-07-29 15:41:28'),(8,2,31,33,'2013-08-05 22:17:32','2013-07-29 15:41:36'),(9,2,26,40,'2013-08-05 22:16:55','2013-07-29 16:19:25'),(10,2,26,40,'2013-08-05 22:16:55','2013-07-29 16:19:24'),(11,2,26,40,'2013-07-29 13:20:12','2013-07-29 16:19:15'),(12,2,26,40,'2013-07-29 13:19:59','2013-07-29 16:18:23'),(13,2,26,32,'2013-08-06 01:06:52','2013-08-06 01:18:49'),(14,2,31,32,'2013-08-06 05:07:02','2013-08-06 04:06:43'),(15,2,26,32,'2013-08-06 04:47:25','2013-08-06 07:47:00'),(16,2,26,33,'2013-08-06 04:47:24','2013-08-06 07:47:03'),(17,2,31,33,'2013-08-06 05:07:04','2013-08-06 07:47:10'),(18,2,31,40,'2013-08-06 05:07:03','2013-08-06 07:47:17'),(19,2,31,32,'2013-08-06 05:07:05','2013-08-06 08:06:48'),(20,2,31,32,'2013-08-06 08:01:36','2013-08-06 11:01:19'),(21,2,31,32,'2013-08-09 12:59:54','2013-08-09 15:59:20'),(22,2,26,32,'2013-08-09 12:59:38','2013-08-09 15:59:25'),(23,2,26,32,'2013-08-09 16:18:09','2013-08-09 19:17:20'),(24,2,26,32,'2013-08-09 16:17:54','2013-08-09 19:17:26'),(25,2,26,32,'2013-08-09 16:42:37','2013-08-09 19:18:53'),(26,1,31,32,NULL,'2013-08-09 19:20:23'),(27,2,26,33,'2013-08-09 16:42:39','2013-08-09 19:20:49'),(28,2,26,33,'2013-08-09 16:42:41','2013-08-09 19:35:27'),(29,2,26,33,'2013-08-09 16:42:41','2013-08-09 19:35:27'),(30,2,26,33,'2013-08-09 16:42:41','2013-08-09 19:36:05'),(31,2,26,33,'2013-08-09 16:42:41','2013-08-09 19:36:26'),(32,2,26,33,'2013-08-09 16:42:43','2013-08-09 19:37:18'),(33,2,26,33,'2013-08-09 16:42:42','2013-08-09 19:38:04'),(34,2,26,33,'2013-08-09 16:42:43','2013-08-09 19:38:15'),(35,2,26,33,'2013-08-09 16:43:44','2013-08-09 19:38:18'),(36,2,26,33,'2013-08-09 16:43:45','2013-08-09 19:38:21'),(37,2,26,40,'2013-08-09 16:42:52','2013-08-09 19:38:24');
/*!40000 ALTER TABLE `locacao` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-08-28  0:22:07

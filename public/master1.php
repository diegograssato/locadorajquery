<?php
if(!isset($menu)){ 
    $menu=true;   
}
?>
<!DOCTYPE html>
<html>
    <head>

        <title>Agenda</title>
        <!-- proper charset -->
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
        <link rel="stylesheet" type="text/css" href="/agenda/public/css/eap.css" />
        <link rel="shortcut icon" href="favicon.ico" />
    </head>
    <body>

        <div id="container">


            <!-- header -->
            <div id="header">


                <!-- logo section -->
                <table border=0 style="height:100%">
                    <tr style="vertical-align:bottom;">	

                        <td class="prod-version">
                            Agenda de Contatos
                        </td>

                    </tr>
                </table>
            </div>
            <?php 
                if($menu == true){ 
            ?>
            <div class="menu" style="margin-top:30px">
                <ul id="menuhor">
                    <li>
                        <a href="/agenda/index.php" title="Entrada no site">Home</a>
                    </li>
                    <li>
                        <a href="/agenda/view/contatos/lista.php" title="Agenda">Lista de Contatos</a>
                    </li>
                    <li>
                        <a href="/agenda/view/contatos/salva.php" title="Agenda">Cadastro Agenda</a>
                    </li>
                    <li>
                        <a href="/agenda/info.php" title="Fale conosco">Contato</a>
                    </li>
                    
                    <li>
                        <a href="/agenda/sair.php" title="Logout">Logout</a>
                    </li>

                </ul>
            </div>
<?php 
    // isset verifica se a sessão chamada Parceiro já existe
   /* session_start();
    if(isset($_SESSION['usuario']) && isset($_SESSION['logado_em'])) {
    echo "<br>&emsp;&emsp;&emsp;&emsp;Usuário logado: '".$_SESSION['usuario']."' ";
        if (time() > $_SESSION['logado_em'] + 3600){
            header('Location: /agenda/sair.php')    ;
        }

    }else{
        header('Location: /agenda/login.php?msg=Sua sessão foi finalizada!');
    }
*/
    } 
?>
            <!-- main content -->
            <div id="content">

                <div class="section">

                    <!-- Faz o carregamento das informacoes no cento da tela -->
                    <?php echo $pagemaincontent; ?>


                </div>

            </div>

            <!-- footer -->
            <div id="footer">
                <span title="Agenda de Contatos"><center>Kees Inform&aacute;tica | Solu&ccedil;&otilde;es Corporativas e Treinamentos</center></span>

            </div>

        </div>

    </body >
</html>

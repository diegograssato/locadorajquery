<?php
$categorias = new categoriaController();

if (($_SERVER['REQUEST_METHOD'] == 'POST') && (!empty($_POST))) {
    $salvar = $categorias->salvar($_POST);
    if ($salvar > 0) {
        header('Location: index.php?control=categorias&pag=list&msg=Registro ' . $salvar . ' alterado/criado com sucesso');
    } else {
        header('Location: index.php?control=categorias&pag=list&msg=Falha ao alterar');
    }
} else {
    if (($_SERVER['REQUEST_METHOD'] == 'GET') && (!empty($_GET['id']))) {
        $id = $_GET['id'];
        $categoria = $categorias->buscaID($id);
    }
    ?>

    <label class="subtitle">Informações da Categoria</label>
    <form id='cadastro' method='POST' action='#'>
        <table>
            <tr>
                <td>
                    <label for="nome">Nome</label>
                </td>
                <td>
                    <?php
                    if ($id > 0) {
                        echo '<input name="id" type="hidden" value="' . $categoria['id'] . '" />';
                    }
                    ?>
                    <input name="nome" type="text" placeholder="Nome" value="<?= $categoria['nome'] ?>" />
                </td>
            </tr>

            <tr>
                <td>
                    <label for="descricao">Descrição</label>
                </td>
                <td>
                    <input name="descricao" type="text" placeholder="Descrição" value="<?= $categoria['descricao'] ?>" />
                </td>
            </tr>

            <tr>
                <td>
                    <input type='submit' class="btn btn-primary" value=' Salvar '>
                </td>
                <td>
                    <input type='reset' class="btn" name='reset' value='Limpar'>
                </td>
            </tr>
        </table>



    </form>


<?php
}
<?php
$categorias = new categoriaController();
if (($_SERVER['REQUEST_METHOD'] == 'GET') && (!empty($_GET['listar']))) {
    $categoria = $categorias->buscaID($_GET['listar']);
    ?>
    <div class="hero-unit">
        <table style="width: 80%;">
            <tr><td>Nome</td><td><?= $categoria['nome']; ?></td></tr>
            <tr><td>Descrição</td><td><?= $categoria['descricao']; ?></td></tr>
            <tr><td>Data de Cadastro</td><td><?php echo date("d/m/Y", strtotime($categoria['criacao'])); ?></td></tr>
            <tr>
                <td> 
                    <a href="index.php?control=categorias&pag=list">Voltar </a> 
                </td>
                <td> 
                    <a href="index.php?control=categorias&pag=zform&id=<?= $categoria['id']; ?>">Editar</a>&emsp;&emsp;
                    <a href="index.php?control=categorias&pag=list&del=<?= $categoria['id']; ?>">Remover</a>
                </td>
            </tr> 
        </table>
    </div>
<?php } ?>
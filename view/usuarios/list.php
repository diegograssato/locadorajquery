<?php
$usuarios = new usuarioController();
$lista = null;

if (($_SERVER['REQUEST_METHOD'] == 'GET') && (!empty($_GET['del']))) {

    if ($usuarios->apagar($_GET['del']) == true) {
        header('Location: index.php?control=usuarios&pag=list&msg=Removido com sucesso!');
    } else {
        header('Location: index.php?control=usuarios&pag=list&msg=Impossivel Remover!');
    }
}

if (($_SERVER['REQUEST_METHOD'] == 'POST') && (!empty($_POST['busca']))) {
    $lista = $usuarios->busca($_POST['busca']);
} else {
    $lista = $usuarios->listar();
}
?>
<fieldset>
    <legend>Listagem de Usuarios</legend>
    <div class="input-append">
        <form id="cadastro" method="post" action="#">
            <input class="span2" style="width: 550px"  id="appendedInputButtons" type="text"name="busca" type="text" placeholder="Buscar">
            <input class="btn" type="submit" value="Buscar"/>
            <a href="index.php?control=usuarios&pag=zform" class="btn btn-primary">Novo Usuário</a>
        </form>

    </div>
    <br>
</fieldset>
<table style="width: 100%;text-align: center" class="table table-hover">
    <!-- Segunda linha -->
    <tr>
        <th> Nome </th>
        <th> E-mail </th>
        <th> Data de Criação </th>
        <th>&emsp; Opções </th>
    </tr>
    <!-- Lista todos os itens e os itens de busca -->
    <?php foreach ($lista as $usuario): //print_r($usuario);  ?>
        <tr>
            <td><?= $usuario['nome']; ?></td>
            <td><?= $usuario['email']; ?></td>
            <td>&ensp;<?php echo date("d/m/Y", strtotime($usuario['criacao'])); ?></td>
            <td><div class="btn-group">
                    <a class="btn" href="index.php?control=usuarios&pag=details&listar=<?= $usuario['id']; ?>"><i class="icon-align-left"></i>Detalhes</a>&emsp;
                    <a class="btn" href="index.php?control=usuarios&pag=zform&id=<?= $usuario['id']; ?>">Editar</a>&emsp;
                    <a class="btn" href="index.php?control=usuarios&pag=newpass&id=<?= $usuario['id']; ?>">Trocar Senha</a>&emsp;
                    <a class="btn" href="index.php?control=usuarios&pag=list&del=<?= $usuario['id']; ?>">Remover</a>
                </div>
            </td>
        </tr>
    <?php endforeach; ?>
</table>
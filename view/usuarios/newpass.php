<?php
$usuarios = new usuarioController();

if (($_SERVER['REQUEST_METHOD'] == 'POST') && (!empty($_POST) && ($_POST['senha'] == $_POST['senha1']) )) {
    $salvar = $usuarios->senha($_POST);
    if ($salvar > 1) {
        header('Location: index.php?control=usuarios&pag=list&msg=Registro ' . $salvar . ' alterado/criado com sucesso');
    } else {
        header('Location: index.php?control=usuarios&pag=list&msg=Falha ao alterar');
    }
} else {
    if (($_SERVER['REQUEST_METHOD'] == 'GET') && (!empty($_GET['id']))) {
        $id = $_GET['id'];
        $usuario = $usuarios->buscaID($id);
    }
    ?>

    <label class="subtitle">Informações do Contato</label>
    <form id='cadastro' method='POST' action='#'>
        <table>
            <tr>

                <td>
                    <?php
                    if ($id > 0) {
                        echo '<input name="id" type="hidden" value="' . $usuario['id'] . '" />';
                    }
                    ?>

                </td>
            </tr>

            <tr>
                <td>
                    <label for="login">Login:</label>
                </td>
                <td>
                    <label for="login"><?= $usuario['email'] ?></label>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="senha">Senha</label>
                </td>
                <td>
                    <input name="senha" type="password" placeholder="Senha" required value="" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="senha1">Repira a Senha</label>
                </td>
                <td>
                    <input name="senha1" type="password" placeholder="Confirmação" required value="" />
                </td>
            </tr>
            <tr>
                <td>
                    <input type='submit' class="btn btn-primary" value="Salvar">
                </td>
                <td>
                    <input type='reset' class="btn" name='reset' value='Limpar'>
                </td>
            </tr>
        </table>



    </form>


    <?php
}
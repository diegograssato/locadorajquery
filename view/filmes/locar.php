<?php
$filmes = new filmeController();


if (($_SERVER['REQUEST_METHOD'] == 'POST') && (!empty($_POST))) {
    $salvar = $filmes->locar($_POST);
    if ($salvar > 0) {
        header('Location: index.php?control=filmes&pag=locacao&msg=Registro ' . $salvar . ' locado com sucesso');
    } else {
        header('Location: index.php?control=filmes&pag=locacao&msg=Falha ao alterar');
    }
} else {
    if (($_SERVER['REQUEST_METHOD'] == 'GET') && (!empty($_GET['id']))) {
        $id = $_GET['id'];
        $filme = $filmes->buscaID($id);
    }
    ?>

    <label class="subtitle">Informações da Filme</label>
    <form id='cadastro' method='POST' action='#'>
        <table>
            <tr>
                <td>
                    <label for="nome">Filme</label>
                </td>
                <td>
                    <?php
                    if ($id > 0) {
                        echo '<input name="id_filme" type="hidden" value="' . $filme['id'] . '" />';
                    }
                    ?>
                    <input name="nome" disabled="disabled" type="text" placeholder="Nome" value="<?= $filme['nome'] ?>" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="id_cliente">Clientes</label>
                </td>
                <td>
                    <select name="id_cliente">
                        <?php foreach ( $filmes->clientes()  as $cli){ ?>
                         <option value="<?= $cli['id'] ?>" ><?= $cli['nome'] ?></option>
                        <?php } ?>
                    </select>
                   
                </td>
            </tr>
             
            <tr>
                <td>
                    <input type='submit' class="btn btn-primary" value=' Locar '>
                </td>
                
            </tr>
        </table>



    </form>


<?php
}
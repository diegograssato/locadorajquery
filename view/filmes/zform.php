<?php
$filmes = new filmeController();


if (($_SERVER['REQUEST_METHOD'] == 'POST') && (!empty($_POST))) {
    $salvar = $filmes->salvar($_POST);
    if ($salvar > 0) {
        header('Location: index.php?control=filmes&pag=list&msg=Registro ' . $salvar . ' alterado/criado com sucesso');
    } else {
        header('Location: index.php?control=filmes&pag=list&msg=Falha ao alterar');
    }
} else {
    if (($_SERVER['REQUEST_METHOD'] == 'GET') && (!empty($_GET['id']))) {
        $id = $_GET['id'];
        $filme = $filmes->buscaID($id);
    }
    ?>

    <label class="subtitle">Informações da Filme</label>
    <form id='cadastro' method='POST' action='#'>
        <table>
            <tr>
                <td>
                    <label for="nome">Nome</label>
                </td>
                <td>
                    <?php
                    if ($id > 0) {
                        echo '<input name="id" type="hidden" value="' . $filme['id'] . '" />';
                    }
                    ?>
                    <input name="nome" type="text" placeholder="Nome" value="<?= $filme['nome'] ?>" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="id_categoria">Categoria</label>
                </td>
                <td>
                    <select name="id_categoria">
                        <?php foreach ( $filmes->categoria()  as $cat){ ?>
                         <option value="<?= $cat['id'] ?>" <?= $filme['id_categoria'] == $cat['id'] ? "selected" :"no" ?>><?= $cat['nome'] ?></option>
                        <?php } ?>
                    </select>
                   
                </td>
            </tr>
            <tr>
                <td>
                    <label for="resumo">Resumo</label>
                </td>
                <td>
                    <input name="resumo" type="text" placeholder="Resumo" value="<?= $filme['resumo'] ?>" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="sinopse">Sinopse</label>
                </td>
                <td>
                    <textarea rows="5" name="sinopse">
                                    <?= $filme['sinopse'] ?>
                    </textarea>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="quantidade">Quantidade</label>
                </td>
                <td>
                    <input name="quantidade" type="number" placeholder="Quantidade" value="<?= $filme['quantidade'] ?>" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="destaque">Destaque</label>
                </td>
                <td>
                    <select name="destaque">
                        <option value="1" <?= $filme['destaque'] == 1? "selected" :"no" ?>>Sim</option>
                        <option value="0" <?= $filme['destaque'] == 0? "selected" :"no" ?>>Não</option>
                        
                    </select>
                   
                </td>
            </tr>
            <tr>
                <td>
                    <label for="status">Status</label>
                </td>
                <td>
                    <select name="status">
                        <option value="1" <?= $filme['status'] == 1? "selected" :"no" ?>>Ativo</option>
                        <option value="0" <?= $filme['status'] == 0? "selected" :"no" ?>>Inativo</option>
                        
                    </select>
                   
                </td>
            </tr>

            <tr>
                <td>
                    <input type='submit' class="btn btn-primary" value=' Salvar '>
                </td>
                <td>
                    <input type='reset' class="btn" name='reset' value='Limpar'>
                </td>
            </tr>
        </table>



    </form>


<?php
}
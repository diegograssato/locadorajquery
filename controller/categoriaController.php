
<?php

class categoriaController {

    private $_categorias;
    public function __construct() {
        $this->_categorias = new Categorias();
    }

    public function listar() {
        return  $this->_categorias->findAll();
    }
  
    public function busca($dados) {
        return  $this->_categorias->find($dados);
    }
    
    public function buscaID($id) {
        return  $this->_categorias->findByID($id);
    }

    public function apagar($id) {
        return  $this->_categorias->remove($id);
    }
    
    public function salvar($dados) {
        $categoria = new Categoria();
        $categoria->setNome($dados['nome']);
        $categoria->setDescricao($dados['descricao']);
        
        if(isset($dados['id']) && $dados['id'] > 0){
            $categoria->setId($dados['id']);
            return  $this->_categorias->update($categoria);
        }else{
           
            return  $this->_categorias->insert($categoria);
        }
    }
    
}


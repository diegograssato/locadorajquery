
<?php

class clienteController {

    private $_contatos;
    public function __construct() {
        $this->_contatos = new Clientes();
    }

    public function listar() {
        return  $this->_contatos->findAll();
    }
  
    public function busca($dados) {
        return  $this->_contatos->find($dados);
    }
    
    public function buscaID($id) {
        return  $this->_contatos->findByID($id);
    }

    public function apagar($id) {
        return  $this->_contatos->remove($id);
    }
    
     public function salvar($dados) {
        $cliente = new Cliente();
        $cliente->setNome($dados['nome']);
        $cliente->setEmail($dados['email']);
        
        if(isset($dados['id']) && $dados['id'] > 0){
            $cliente->setId($dados['id']);
             return  $this->_contatos->update($cliente);
        }else{
           
            return  $this->_contatos->insert($cliente);
        }
    }
}

